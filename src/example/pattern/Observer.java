package example.pattern;

/**
 * @author Alessandro
 */
public interface Observer {
	
	void update(Subject observable, Object ... newState);
}
