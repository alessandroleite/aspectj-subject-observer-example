package example.aspects;

import example.pattern.Subject;
import example.types.Person;

/**
 * @author Alessandro
 */
public aspect PersonAspect extends ObserverPattern {

	declare parents: Person implements Subject;

	public pointcut notifyObservers(Subject observable, Object newState):  
		 target (observable)  && (execution (* *.set*(..)))  && args(newState);

//	pointcut notifyObservers2(Subject observable, Object ... newState): 
//		target (observable) && (execution (* *.set*(..))) && args(newState);
//
//	after(Subject observable, Object[] newState) returning: notifyObservers2(observable, newState) {
//		for (Observer obs : observable.getObservers()) {
//			obs.update(observable, newState);
//		}
//	}
}
