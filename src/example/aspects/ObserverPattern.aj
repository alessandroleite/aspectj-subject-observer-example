package example.aspects;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;

import example.pattern.Observer;
import example.pattern.Subject;


/**
 * @author Alessandro
 */
public abstract aspect ObserverPattern {

	public abstract pointcut notifyObservers(Subject observable, Object newState);

	/*after(Subject observable, Object newState) returning : notifyObservers(observable, newState) {
		for (Observer obs : observable.getObservers()) {
			obs.update(observable, newState);
		}
	}*/
	
	Object around(Subject observable, Object newState) : notifyObservers(observable, newState){
		Object advicedMethodReturn;
		
		synchronized (thisJoinPoint.getTarget()) {
			advicedMethodReturn = proceed(observable, newState);	
		}
		
		for (Observer obs : observable.getObservers()) {
			obs.update(observable, newState, advicedMethodReturn);
		}
		return advicedMethodReturn;
	}

	// Subject inter-type declaration

	private final Collection<Observer> Subject.observers = new CopyOnWriteArrayList<Observer>();

	public void Subject.attach(Observer observer) {
		if (observer != null && !observers.contains(observer)) {
			observers.add(observer);
		}
	}

	public void Subject.detach(Observer observer) {
		if (observer != null) {
			observers.remove(observer);
		}
	}

	public Collection<Observer> Subject.getObservers() {
		return Collections.unmodifiableCollection(this.observers);
	}
}