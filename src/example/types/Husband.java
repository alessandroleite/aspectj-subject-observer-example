package example.types;

import java.util.Arrays;

import example.pattern.Observer;
import example.pattern.Subject;

/**
 * @author Alessandro
 */
public class Husband extends Person implements Observer {

	private Person wife;

	public Husband(String name, Person wife) {
		super(name);
		this.wife = wife;
		wife.attach(this);
	}

	@Override
	public void update(Subject observable, Object... newState) {
		System.out.print(observable + " has a new state: ");

		Object[] states = (Object[]) newState[0];

		for (Object state : states) {
			System.out.print(state + " ");
		}
		System.out.println();
		
		if (Arrays.binarySearch(states, "TPM") > -1 || Arrays.binarySearch(states, "Angry") > -1) {
			System.out.println(this.getName() + " pay attention to your own safety!");
		}
	}

	public Person getWife() {
		return wife;
	}
}