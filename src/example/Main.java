package example;

import example.types.Husband;
import example.types.Person;

/**
 * @author Alessandro
 */
public class Main {

	public static void main(String[] args) {

		Person wife = new Person("B");
		new Husband("A", wife);
		wife.setEmotions("TPM");
	}
}
